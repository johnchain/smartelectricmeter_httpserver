const express = require('express')
const mysql = require('mysql')
const router = express.Router()

router.post('/account_create', (req, res) => {
        console.log("Trying to create a new user...")

        const status = req.body.status
        const state = req.body.state
        const firstName = req.body.firstName
        const lastName = req.body.lastName
        const phoneNumber = req.body.phoneNumber
        const PID = req.body.PID
        const address = req.body.address

        const checkExistString = "select count(*) from accounts where phoneNumber = ?"
        getConnection().query(checkExistString, [phoneNumber], (err, rows, fields) => {
                if (err) {
                        console.log("Failed to query for meter: " + err)
                        res.sendStatus(500)
                        return
                }
                flag = rows[0]["count(*)"]
                if (flag == 0){
                        const queryString = "insert into accounts (status, state, firstName, lastName, phoneNumber, PID, address) values(?, ?, ?, ?, ?, ?, ?)"
                        getConnection().query(queryString, [status, state, firstName, lastName, phoneNumber, PID, address], (err, results, fields) => {
                                if (err){
                                        console.log("Failed to insert account: " + err)
                                        res.sendStatus(500)
                                        return 
                                }
                                res.send("Inserted a new account with id: " + results.insertId)
                        })
                }else{
                        const queryString = "update accounts set status = ?, state = ?, firstName = ?, lastName = ?, PID = ?, address = ? where phoneNumber = ?"
                        getConnection().query(queryString, [status, state, firstName, lastName, PID, address, phoneNumber], (err, results, fields) => {
                                if (err){
                                        console.log("Failed to update account: " + err)
                                        res.sendStatus(500)
                                        return 
                                }
                                res.send("update one account with id: " + results.insertId)
                        })
                }
        })
})

router.get('/user/:idd', (req, res) => {
        console.log("Fetching user with id: " + req.params.idd)
        const connection = getConnection()
        const userId = req.params.idd
        const queryString = "select * from accounts where accountID = ?"
        connection.query(queryString, [userId], (err, rows, fields) => {
                if (err) {
                        console.log("Failed to query for users: " + err)
                        res.sendStatus(500)
                        return
                }

                // const account = rows.map((row) => {
                //         return {firstName: row.firstName, phone: row.phoneNumber}
                // })
                // res.json(account)
                res.json(rows)
        })
})

router.get("/users", (req, res) => {
        const connection = getConnection()
        const userId = req.params.idd
        const queryString = "select * from accounts"
        connection.query(queryString, (err, rows, fields) => {
                if (err) {
                        console.log("Failed to query for users: " + err)
                        res.sendStatus(500)
                        return
                }
                res.json(rows)
        })
})

router.post('/meter_create', (req, res) => {
        console.log("Trying to create a new meter...")
        flag = -1;
        const mac = req.body.mac
        const type = req.body.type
        const state = req.body.state
        const updateTimes = req.body.updateTimes
        const description = req.body.description
        const checkExistString = "select count(*) from meters where mac = ?"
        getConnection().query(checkExistString, [mac], (err, rows, fields) => {
                if (err) {
                        console.log("Failed to query for meter: " + err)
                        res.sendStatus(500)
                        return
                }
                flag = rows[0]["count(*)"]
                if (flag == 0){
                        const queryString = "insert into meters (mac, type, state, updateTimes, description) values(?, ?, ?, ?, ?)"
                        getConnection().query(queryString, [mac, type, state, updateTimes, description], (err, results, fields) => {
                                if (err){
                                        console.log("Failed to insert meter: " + err)
                                        res.sendStatus(500)
                                        return 
                                }
                                res.send("Inserted a new meter with id: " + results.insertId)
                        })
                }else{
                        const queryString = "update meters set type = ?, state = ?, updateTimes = ?, description = ? where mac = ?"
                        getConnection().query(queryString, [type, state, updateTimes, description, mac], (err, results, fields) => {
                                if (err){
                                        console.log("Failed to update meter: " + err)
                                        res.sendStatus(500)
                                        return 
                                }
                                res.send("update one meter with id: " + results.insertId)
                        })
                }
        })
})

router.post('/meter_delete', (req, res) => {
        console.log("Trying to delete a meter...")
        flag = -1;
        const mac = req.body.mac

        const queryString = "delete from meters where mac = ?"
        getConnection().query(queryString, [mac], (err, results, fields) => {
                if (err){
                        console.log("Failed to insert meter: " + err)
                        res.sendStatus(500)
                        return
                }
                res.send("delete a meter with id: " + results.insertId)
        })
})

router.post('/meter_unbind', (req, res) => {
        //不能正确解析json 格式的post参数
        var body = '', jsonStr = null;
        req.on('data', function (chunk) {
                body += chunk; //读取参数流转化为字符串
        });
        req.on('end', function () {
                //读取参数流结束后将转化的body字符串解析成 JSON 格式
                try {
                        jsonStr = JSON.parse(body);
                } catch (err) {
                        jsonStr = null;
                }
                if(jsonStr.function == "unbind" && jsonStr.key){
                        const queryString = "delete from meterPosition where meterID = ?"
                        getConnection().query(queryString, [jsonStr.key], (err, results, fields) => {
                                if (err){
                                        console.log("Failed to insert meter: " + err)
                                        res.sendStatus(500)
                                        return
                                }
                                res.send("delete a meter with id: " + results.insertId)
                        })
                }else{
                        res.send({"status":"error"})
                }
        });
})

router.post('/meter_switch', (req, res) => {
        //不能正确解析json 格式的post参数
        var body = '', jsonStr = null;
        req.on('data', function (chunk) {
                body += chunk; //读取参数流转化为字符串
        });
        req.on('end', function () {
                //读取参数流结束后将转化的body字符串解析成 JSON 格式
                try {
                        jsonStr = JSON.parse(body);
                } catch (err) {
                        jsonStr = null;
                }
                if(jsonStr.function == "switch" && jsonStr.key && jsonStr.value){
                        console.log("will post to python server")
                        var request = require('request');
                        request({
                                url: "http://127.0.0.1:8080/cmd",
                                method: "POST",
                                json: true,   // <--Very important!!!
                                body: jsonStr
                                }, function (error, response, body){
                                        console.log(body);
                                        var rst = null;
                                        if(body.rst){
                                                rst = body.rst;
                                        }
                                        res.send({"status":rst});
                                }
                        );
                }else{
                        res.send({"status":"error"})
                }
        });
})

router.post('/meter2user', (req, res) => {
        console.log("Trying to bind meter to user...")
        flag = -1
        const meterID = req.body.meterID
        const accountID = req.body.accountID

        const checkExistString = "select count(*) from meterOwnerShip where meterID = ?"
        getConnection().query(checkExistString, [meterID], (err, rows, fields) => {
                if (err) {
                        console.log("Failed to query for meter: " + err)
                        res.sendStatus(500)
                        return
                }
                flag = rows[0]["count(*)"]
                if (flag == 0){
                        const queryString = "insert into meterOwnerShip (meterID, accountID) values(?, ?)"
                        getConnection().query(queryString, [meterID, accountID], (err, results, fields) => {
                                if (err){
                                        console.log("Failed to insert meterOwnerShip: " + err)
                                        res.sendStatus(500)
                                        return
                                }
                                res.send("Inserted a new meterOwnerShip with id: " + results.insertId)
                        })
                }else{
                        const queryString = "update meterOwnerShip set accountID = ? where meterID = ?"
                        getConnection().query(queryString, [accountID, meterID], (err, results, fields) => {
                                if (err){
                                        console.log("Failed to insert meterOwnerShip: " + err)
                                        res.sendStatus(500)
                                        return 
                                }
                                res.send("update one meterOwnerShip with id: " + results.insertId)
                        })
                }
        })
})

router.post('/meter2house', (req, res) => {
        console.log("meter2house:" + req.body)
        meterID = -1
        const mac = req.body.mac
        const city = req.body.city
        const province = req.body.province
        const community = req.body.community
        const house = req.body.house

        var checkExistString = "select meterID from meters where mac = ?"
        getConnection().query(checkExistString, [mac], (err, rows, fields) => {
                if (err) {
                        console.log("Failed to query for meter2house: " + err)
                        res.sendStatus(500)
                        return
                }
                if(rows.length > 0){
                        console.log(rows)
                        meterID = rows[0]["meterID"]
                }else{
                        var body = {"status": -1, "reason": "mac not found: " + mac}
                        res.send(JSON.stringify(body))
                        return
                }

                console.log("here2 " + meterID)
                checkExistString = "select meterID from meterPosition where meterID = ?"
                getConnection().query(checkExistString, [meterID], (err, rows, fields) => {
                        if (err) {
                                console.log("Failed to query for meter2house: " + err)
                                res.sendStatus(500)
                                return
                        }
                        console.log("rows.length = " + rows.length + " rows = " + rows)
                        if (rows.length == 0){
                                const queryString = "insert into meterPosition (meterID, province, city, community, house) values(?, ?, ?, ?, ?)"
                                getConnection().query(queryString, [meterID, province, city, community, house], (err, results, fields) => {
                                        if (err){
                                                console.log("Failed to insert meter2house: " + err)
                                                res.sendStatus(500)
                                                return
                                        }
                                        var body = {"status": 0, "reason": "Inserted a new meter2house with id: " + results.insertId}
                                        res.send(JSON.stringify(body))
                                })
                        }else{
                                const queryString = "update meterPosition set province = ?, city = ?, community = ?, house = ? where meterID = ?"
                                getConnection().query(queryString, [province, city, community, house, meterID], (err, results, fields) => {
                                        if (err){
                                                console.log("Failed to update meter2house: " + err)
                                                res.sendStatus(500)
                                                return
                                        }
                                        var body = {"status": 0, "reason": "update one meter2house with id: " + results.insertId}
                                        res.send(JSON.stringify(body))
                                })
                        }
                })
        })
})

router.get("/meters", (req, res) => {
        const connection = getConnection()
        const userId = req.params.idd
        const queryString = "select * from meterPosition"
        connection.query(queryString, (err, rows, fields) => {
                if (err) {
                        console.log("Failed to query for meter: " + err)
                        res.sendStatus(500)
                        return
                }
                res.json(rows)
        })
})

router.get("/meter/all", (req, res) => {
        const connection = getConnection()
        const queryString = "select * from meters"
        connection.query(queryString, (err, rows, fields) => {
                if (err) {
                        console.log("Failed to query for meter: " + err)
                        res.sendStatus(500)
                        return
                }
                res.json(rows)
        })
})

router.get('/meter/:idd', (req, res) => {
        console.log("Fetching meter with id: " + req.params.idd)
        const connection = getConnection()
        const meterId = req.params.idd
        const queryString = "select * from meters where meterID = ?"
        connection.query(queryString, [meterId], (err, rows, fields) => {
                if (err) {
                        console.log("Failed to query for meter: " + err)
                        res.sendStatus(500)
                        return
                }
                res.json(rows)
        })
})

router.post('/cmd_send', (req, res) => {
        const mac = req.body.mac
        const cmdID = req.body.cmdID
        const value = req.body.value
        console.log("Trying to send cmd... " + mac + "  " + cmdID + "  " + value)
        res.send("cmd sent")
})

// router.get("/", (req, res) => {
//         res.send("Hello from ROOOOT")
// })

const pool = mysql.createPool({
        connectionLimit: 20,    //TODO:
        host: '127.0.0.1',
        user: 'root',
        password: '123456',
        database: 'SmartElectricMeter'
})

function getConnection(){
        return pool
}

module.exports = router