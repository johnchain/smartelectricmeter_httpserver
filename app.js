
const express = require('express')
const app = express()
const morgan = require('morgan')
const mysql  = require('mysql')
const bodyParser = require('body-parser')
const os = require('os');

app.use(bodyParser.urlencoded({extended: false}))
app.use(morgan('combined'))  //combined/short
app.use(express.static('./public'))

const router = require('./routers/user.js')
app.use(router)


var host = ""
if (os.platform() == 'darwin'){
        host = "127.0.0.1"
}else{
        host = "www.asuscn.space"
}
app.listen(3003, host, 100, () => {
        console.log("server is up and listening on 3003...")
})