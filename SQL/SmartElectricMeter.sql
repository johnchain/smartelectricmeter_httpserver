# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.22)
# Database: SmartElectricMeter
# Generation Time: 2018-06-19 13:20:41 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table accounts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accounts`;

CREATE TABLE `accounts` (
  `accountID` bigint(64) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `status` int(11) DEFAULT NULL COMMENT '帐户类型',
  `state` int(11) DEFAULT NULL COMMENT '账户状态',
  `firstName` char(10) DEFAULT NULL COMMENT '名',
  `lastName` char(10) DEFAULT NULL COMMENT '姓',
  `phoneNumber` char(13) DEFAULT NULL COMMENT '手机号',
  `PID` char(18) DEFAULT NULL COMMENT '身份证号',
  `address` char(250) DEFAULT NULL COMMENT '联系地址',
  `lastActiveTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近信息变动时间',
  `reserve1` varchar(50) DEFAULT NULL,
  `reserve2` int(11) DEFAULT NULL,
  `reserve3` int(11) DEFAULT NULL,
  PRIMARY KEY (`accountID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;

INSERT INTO `accounts` (`accountID`, `status`, `state`, `firstName`, `lastName`, `phoneNumber`, `PID`, `address`, `lastActiveTime`, `reserve1`, `reserve2`, `reserve3`)
VALUES
	(1,1,1,'johnchain','lee','18556732048',NULL,NULL,'2018-05-05 23:49:36',NULL,NULL,NULL),
	(2,1,2,'xiaojun','chen','18862485260',NULL,NULL,'2018-05-05 23:49:51',NULL,NULL,NULL),
	(3,2,3,'johnchain3','lee','18862485260','3207221991',NULL,'2018-05-06 11:24:08',NULL,NULL,NULL),
	(4,4,2,'johnchian4','lee','18556732048','111111','gaoxinqu','2018-05-15 23:15:47',NULL,NULL,NULL),
	(5,1,5,'johnchian5','lee','18556732048','3207221991','gaoxinqu','2018-06-19 21:15:52',NULL,NULL,NULL),
	(6,2,6,'johnchian6','lee','18556732048','3207221991','gaoxinqu','2018-06-19 21:15:53',NULL,NULL,NULL),
	(7,2,7,'johnchian7','lee','18556732048','3207221991','gaoxinqu','2018-06-19 21:15:54',NULL,NULL,NULL),
	(8,3,8,'johnchain8','lee','18556732048','','gaoxinqu','2018-06-19 21:15:56',NULL,NULL,NULL),
	(9,3,9,'johnchain9','lee','18556732048','','gaoxinqu','2018-06-19 21:15:57',NULL,NULL,NULL),
	(10,3,10,'johnchain0','lee','18556732048','','gaoxinqu','2018-06-19 21:15:59',NULL,NULL,NULL);

/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table historyMeterData
# ------------------------------------------------------------

DROP TABLE IF EXISTS `historyMeterData`;

CREATE TABLE `historyMeterData` (
  `id` bigint(64) unsigned NOT NULL AUTO_INCREMENT,
  `meterID` bigint(64) DEFAULT NULL COMMENT '电表ID',
  `insertTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '插入时间',
  `value` int(32) DEFAULT NULL COMMENT '电表示数',
  `reserve1` int(32) DEFAULT NULL,
  `reserve2` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table meterOwnerShip
# ------------------------------------------------------------

DROP TABLE IF EXISTS `meterOwnerShip`;

CREATE TABLE `meterOwnerShip` (
  `meterID` bigint(64) NOT NULL COMMENT '电表ID，外键参考meters表主键',
  `accountID` bigint(64) NOT NULL COMMENT '账户ID，外键参考accounts表主键',
  `changeTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '变更时间',
  `reserve1` int(32) DEFAULT NULL,
  `reserve2` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`meterID`,`accountID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table meterPosition
# ------------------------------------------------------------

DROP TABLE IF EXISTS `meterPosition`;

CREATE TABLE `meterPosition` (
  `meterID` bigint(64) unsigned NOT NULL,
  `province` varchar(20) NOT NULL DEFAULT '',
  `city` varchar(20) NOT NULL DEFAULT '',
  `community` varchar(40) NOT NULL DEFAULT '',
  `house` varchar(20) NOT NULL DEFAULT '',
  `changeTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reserved1` int(32) DEFAULT NULL,
  `reserved2` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`meterID`),
  CONSTRAINT `meterposition_ibfk_1` FOREIGN KEY (`meterID`) REFERENCES `meters` (`meterID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `meterPosition` WRITE;
/*!40000 ALTER TABLE `meterPosition` DISABLE KEYS */;

INSERT INTO `meterPosition` (`meterID`, `province`, `city`, `community`, `house`, `changeTime`, `reserved1`, `reserved2`)
VALUES
	(1,'江苏省','苏州市','嘉业阳光假日','xx-xx1','2018-06-19 16:43:38',NULL,NULL),
	(2,'江苏省','苏州市','嘉业阳光假日','xx-xx2','2018-06-19 20:44:18',NULL,NULL),
	(3,'江苏省','苏州市','嘉业阳光假日','xx-xx3','2018-06-19 20:51:08',NULL,NULL),
	(4,'江苏省','苏州市','新创竹园','48-506','2018-06-18 20:54:34',NULL,NULL);

/*!40000 ALTER TABLE `meterPosition` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table meters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `meters`;

CREATE TABLE `meters` (
  `meterID` bigint(64) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `mac` varchar(24) NOT NULL DEFAULT '''''' COMMENT 'mac地址',
  `shotAddr` smallint(16) unsigned NOT NULL DEFAULT '0',
  `type` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '电表类型',
  `value` int(32) unsigned NOT NULL DEFAULT '0' COMMENT '电表示数',
  `state` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '电表状态',
  `description` varchar(100) NOT NULL DEFAULT '''''' COMMENT '电表描述',
  `updateTimes` int(32) unsigned NOT NULL DEFAULT '0' COMMENT '刷新次数',
  `lastUpdateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近更新时间',
  `reserve1` varchar(32) DEFAULT NULL,
  `reserve2` int(32) DEFAULT NULL,
  PRIMARY KEY (`meterID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `meters` WRITE;
/*!40000 ALTER TABLE `meters` DISABLE KEYS */;

INSERT INTO `meters` (`meterID`, `mac`, `shotAddr`, `type`, `value`, `state`, `description`, `updateTimes`, `lastUpdateTime`, `reserve1`, `reserve2`)
VALUES
	(1,'bd02ac14004b1200',0,1,1,204,'dianbiao',6,'2018-06-19 21:14:21',NULL,NULL),
	(2,'bd02ac14004b1201',0,0,1,1,'--!',1,'2018-06-19 21:14:20',NULL,NULL),
	(3,'bd02ac14004b1202',0,0,1,1,'desc+++++++',1,'2018-06-19 21:14:19',NULL,NULL),
	(4,'ff02ac14004b12ff',0,0,1,0,'description',0,'2018-06-19 21:14:17',NULL,NULL),
	(5,'ee02ac14004b12ee',0,0,1,0,'description',0,'2018-06-19 21:14:18',NULL,NULL);

/*!40000 ALTER TABLE `meters` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
